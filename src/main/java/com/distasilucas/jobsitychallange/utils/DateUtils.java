package com.distasilucas.jobsitychallange.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

import static com.distasilucas.jobsitychallange.constants.Constants.BIRTHDAY_DATE_FORMAT;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {

    public static LocalDate parseDate(String date, String format) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
    }

    public static LocalDate getBirthDate(String birthday) {
        LocalDate localDate = parseDate(birthday, BIRTHDAY_DATE_FORMAT);

        return isBirthDay29OfFebruary(localDate) ?
                LocalDate.of(localDate.getYear(), localDate.getMonth(), 28) :
                localDate;
    }

    private static boolean isBirthDay29OfFebruary(LocalDate localDate) {
        return Month.FEBRUARY.equals(localDate.getMonth()) && localDate.getDayOfMonth() == 29;
    }
}
