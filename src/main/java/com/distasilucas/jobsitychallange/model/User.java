package com.distasilucas.jobsitychallange.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class User {

    private String firstName;
    private String lastName;
    private LocalDate birthDay;
    private String email;
    private String phoneNumber;
}
