package com.distasilucas.jobsitychallange.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String BIRTHDAY_DATE_FORMAT = "yyyy/MM/dd";
    public static final String EMAIL_BIRTHDAY_SUBJECT = "Happy Birthday!";
    public static final String EMAIL_BIRTHDAY_CONTENT = "Happy birthday, dear %s !";
    public static final String BIRTHDAY_MESSAGE = """
            
            ------------------------------------------------------------------------------
            Subject: %s
            Content: %s
            ------------------------------------------------------------------------------
            """;

    public static final String BIRTHDAY_REMINDER_MESSAGE = """
            
            ------------------------------------------------------------------------------
            Subject: Birthday Reminder
            Dear: %s
            Today is %s birthday.
            Don't forget to send him/her a message!
            ------------------------------------------------------------------------------
            """;
}
