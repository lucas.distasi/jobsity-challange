package com.distasilucas.jobsitychallange.scheduler;

import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.FileReaderService;
import com.distasilucas.jobsitychallange.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.distasilucas.jobsitychallange.constants.Constants.BIRTHDAY_MESSAGE;
import static com.distasilucas.jobsitychallange.constants.Constants.BIRTHDAY_REMINDER_MESSAGE;
import static com.distasilucas.jobsitychallange.constants.Constants.EMAIL_BIRTHDAY_CONTENT;
import static com.distasilucas.jobsitychallange.constants.Constants.EMAIL_BIRTHDAY_SUBJECT;

@Slf4j
@Component
@RequiredArgsConstructor
public class BirthdayScheduler {

    private final FileReaderService fileReaderService;
    private final UserService userService;
    private final BirthdayMessageService birthdayEmailMessageServiceImpl;
    private final BirthdayMessageService birthdayPhoneMessageServiceImpl;

    @Scheduled(cron = "0 0 12 * * ?")
    public void sendMessages() {
        log.info("Executing cron to send messages...");

        List<User> birthdayUsersFromFile = new ArrayList<>();
        List<User> nonBirthdayUsersFromFile = new ArrayList<>();
        List<User> birthdayUsersFromDatabase = new ArrayList<>();
        List<User> nonBirthdayUsersFromDatabase = new ArrayList<>();

        fileReaderService.getBirthdayUsersFromFile("birthdays.txt")
                .forEach(user -> {
                    if (isTodayBirthday(user.getBirthDay())) {
                        birthdayUsersFromFile.add(user);
                    } else {
                        nonBirthdayUsersFromFile.add(user);
                    }
                });

        userService.findAllUsers()
                .forEach(user -> {
                    if (isTodayBirthday(user.getBirthDay())) {
                        birthdayUsersFromDatabase.add(user);
                    } else {
                        nonBirthdayUsersFromDatabase.add(user);
                    }
                });

        sendEmailBirthDayReminder(nonBirthdayUsersFromFile, birthdayUsersFromFile);
        sendPhoneBirthDayReminder(nonBirthdayUsersFromDatabase, birthdayUsersFromDatabase);

        sendPhoneBirthDayMessage(birthdayUsersFromDatabase);
        sendEmailBirthDayMessage(birthdayUsersFromFile);
    }

    private void sendEmailBirthDayReminder(List<User> nonBirthdayUsersFromFile,
                                           List<User> birthdayUsersFromDatabase) {
        List<String> peopleBirthday = birthdayUsersFromDatabase.stream()
                .map(user -> user.getFirstName() + " " + user.getLastName())
                .toList();

        nonBirthdayUsersFromFile.forEach(user -> {
            log.info("Sending birthday [EMAIL] reminder message to {} {}", user.getLastName(), user.getFirstName());
            String content = String.format(BIRTHDAY_REMINDER_MESSAGE, user.getFirstName(), peopleBirthday);

            birthdayEmailMessageServiceImpl.sendBirthDayMessage(user.getEmail(), content);
        });
    }

    private void sendPhoneBirthDayReminder(List<User> nonBirthdayUsersFromDatabase,
                                           List<User> birthdayUsersFromFile) {
        List<String> peopleBirthday = birthdayUsersFromFile.stream()
                .map(user -> user.getFirstName() + " " + user.getLastName())
                .toList();

        nonBirthdayUsersFromDatabase.forEach(user -> {
            log.info("Sending birthday [PHONE] reminder message to {} {}", user.getLastName(), user.getFirstName());
            String content = String.format(BIRTHDAY_REMINDER_MESSAGE, user.getFirstName(), peopleBirthday);

            birthdayPhoneMessageServiceImpl.sendBirthDayMessage(user.getPhoneNumber(), content);
        });
    }

    private void sendPhoneBirthDayMessage(List<User> birthdayUsersFromDatabase) {
        birthdayUsersFromDatabase.forEach(user -> {
            log.info("Sending birthday [PHONE] message to {} {}", user.getLastName(), user.getFirstName());
            String birthdayPersona = String.format(EMAIL_BIRTHDAY_CONTENT, user.getFirstName());
            String content = String.format(BIRTHDAY_MESSAGE, EMAIL_BIRTHDAY_SUBJECT, birthdayPersona);

            birthdayPhoneMessageServiceImpl.sendBirthDayMessage(user.getPhoneNumber(), content);
        });
    }

    private void sendEmailBirthDayMessage(List<User> birthdayUsersFromFile) {
        birthdayUsersFromFile.forEach(user -> {
            log.info("Sending birthday [EMAIL] message to {}", user.getEmail());
            String birthdayPerson = String.format(EMAIL_BIRTHDAY_CONTENT, user.getFirstName());
            String content = String.format(BIRTHDAY_MESSAGE, EMAIL_BIRTHDAY_SUBJECT, birthdayPerson);

            birthdayEmailMessageServiceImpl.sendBirthDayMessage(user.getEmail(), content);
        });
    }

    private static boolean isTodayBirthday(LocalDate birthday) {
        return LocalDate.now().getMonth().equals(birthday.getMonth()) && LocalDate.now().getDayOfMonth() == birthday.getDayOfMonth();
    }
}
