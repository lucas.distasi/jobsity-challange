package com.distasilucas.jobsitychallange.mapper.impl;

import com.distasilucas.jobsitychallange.entity.UserEntity;
import com.distasilucas.jobsitychallange.mapper.EntityMapper;
import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.utils.DateUtils;
import org.springframework.stereotype.Service;

@Service
public class UserModelMapper implements EntityMapper<User, UserEntity> {

    @Override
    public User mapFrom(UserEntity from) {
        return User.builder()
                .firstName(from.getFirstName())
                .lastName(from.getLastName())
                .birthDay(DateUtils.getBirthDate(from.getBirthDay()))
                .email(from.getEmail())
                .phoneNumber(from.getPhoneNumber())
                .build();
    }
}
