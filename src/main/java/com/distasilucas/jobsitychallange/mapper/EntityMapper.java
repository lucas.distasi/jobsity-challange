package com.distasilucas.jobsitychallange.mapper;

public interface EntityMapper<T, U> {

    T mapFrom(U from);
}
