package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.entity.UserEntity;
import com.distasilucas.jobsitychallange.mapper.EntityMapper;
import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.repository.UserRepository;
import com.distasilucas.jobsitychallange.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final EntityMapper<User, UserEntity> userUserEntityEntityMapper;

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userUserEntityEntityMapper::mapFrom)
                .toList();
    }
}
