package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.providers.EmailMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.distasilucas.jobsitychallange.constants.Constants.EMAIL_BIRTHDAY_SUBJECT;

@Service
@RequiredArgsConstructor
public class BirthdayEmailMessageServiceImpl implements BirthdayMessageService {

    private final EmailMessageService gmailProviderService;

    @Override
    public String sendBirthDayMessage(String destination, String content) {
        return gmailProviderService.sendEmail(destination, EMAIL_BIRTHDAY_SUBJECT, content);
    }
}
