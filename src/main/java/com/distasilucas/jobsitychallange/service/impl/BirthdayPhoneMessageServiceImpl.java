package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.providers.PhoneMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BirthdayPhoneMessageServiceImpl implements BirthdayMessageService {

    private final PhoneMessageService twilioProviderService;

    @Override
    public String sendBirthDayMessage(String destination, String content) {
        return twilioProviderService.sendMessage(destination, content);
    }
}
