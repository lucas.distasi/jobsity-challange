package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.service.FileReaderService;
import com.distasilucas.jobsitychallange.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

@Slf4j
@Service
public class FileReaderServiceImpl implements FileReaderService {

    @Override
    public List<User> getBirthdayUsersFromFile(String file) {
        File usersFile = new File(file);
        List<User> users;

        try(Scanner scanner = new Scanner(usersFile)) {
            scanner.nextLine();

            users = new ArrayList<>();

            while (scanner.hasNextLine()) {
                String userLine = scanner.nextLine();
                String[] fields = userLine.split("\\s+");
                String lastName = fields[0];
                String firstName = fields[1];
                String dateOfBirth = fields[2];
                String email = fields[3];
                String phoneNumber = fields[4];

                User user = User.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .birthDay(DateUtils.getBirthDate(dateOfBirth))
                        .email(email)
                        .phoneNumber(phoneNumber)
                        .build();

                users.add(user);
            }
        } catch (FileNotFoundException ex) {
            log.warn("A FileNotFoundException has occurred");
            users = Collections.emptyList();
        } catch (Exception ex) {
            log.error("An unknown Exception has occurred");
            users = Collections.emptyList();
        }

        return users;
    }

}
