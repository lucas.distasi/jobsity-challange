package com.distasilucas.jobsitychallange.service;

import com.distasilucas.jobsitychallange.model.User;

import java.util.List;

public interface FileReaderService {

    List<User> getBirthdayUsersFromFile(String file);
}
