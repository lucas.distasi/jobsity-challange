package com.distasilucas.jobsitychallange.service.providers;

public interface PhoneMessageService {

    String sendMessage(String destination, String content);
}
