package com.distasilucas.jobsitychallange.service.providers;

public interface EmailMessageService {

    String sendEmail(String destination, String subject, String content);
}
