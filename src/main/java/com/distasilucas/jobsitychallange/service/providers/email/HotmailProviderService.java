package com.distasilucas.jobsitychallange.service.providers.email;

import com.distasilucas.jobsitychallange.service.providers.EmailMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HotmailProviderService implements EmailMessageService {

    @Override
    public String sendEmail(String destination, String subject, String content) {
        log.info(String.format("Sending [EMAIL] to %s through [HOTMAIL]", destination));

        return "HotmailProviderService";
    }
}
