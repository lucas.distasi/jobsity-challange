package com.distasilucas.jobsitychallange.service.providers.phone;

import com.distasilucas.jobsitychallange.service.providers.PhoneMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TelesignProviderService implements PhoneMessageService {

    @Override
    public String sendMessage(String destination, String content) {
        log.info("Sending [PHONE] message to {} through [TELESIGN]", destination);
        log.info(content);

        return "TelesignProviderService";
    }
}
