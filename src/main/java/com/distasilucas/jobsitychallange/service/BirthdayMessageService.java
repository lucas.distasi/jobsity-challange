package com.distasilucas.jobsitychallange.service;

public interface BirthdayMessageService {

    String sendBirthDayMessage(String destination, String content);
}
