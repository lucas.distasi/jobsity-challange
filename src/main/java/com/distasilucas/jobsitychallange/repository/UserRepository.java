package com.distasilucas.jobsitychallange.repository;

import com.distasilucas.jobsitychallange.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, String> {
}
