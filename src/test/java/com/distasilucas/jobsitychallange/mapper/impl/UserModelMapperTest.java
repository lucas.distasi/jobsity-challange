package com.distasilucas.jobsitychallange.mapper.impl;

import com.distasilucas.jobsitychallange.entity.UserEntity;
import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.utils.DateUtils;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeFormatter;

import static com.distasilucas.jobsitychallange.constants.Constants.BIRTHDAY_DATE_FORMAT;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserModelMapperTest {

    UserModelMapper userModelMapper = new UserModelMapper();

    @Test
    void shouldMapSuccessfully() {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail("sophia.patel@hotmail.com");
        userEntity.setFirstName("Sophia");
        userEntity.setLastName("Patel");
        userEntity.setBirthDay("1989/12/06");
        userEntity.setPhoneNumber("555-123-4567");

        User user = userModelMapper.mapFrom(userEntity);
        String expectedDate = DateUtils.parseDate(userEntity.getBirthDay(), BIRTHDAY_DATE_FORMAT)
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        assertAll(
                () -> assertEquals(userEntity.getFirstName(), user.getFirstName()),
                () -> assertEquals(userEntity.getLastName(), user.getLastName()),
                () -> assertEquals(userEntity.getEmail(), user.getEmail()),
                () -> assertEquals(userEntity.getPhoneNumber(), user.getPhoneNumber()),
                () -> assertEquals(expectedDate, user.getBirthDay().toString())
        );
    }

}