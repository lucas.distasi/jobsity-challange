package com.distasilucas.jobsitychallange.scheduler;

import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.FileReaderService;
import com.distasilucas.jobsitychallange.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BirthdaySchedulerTest {

    @Mock
    FileReaderService fileReaderServiceMock;

    @Mock
    UserService userServiceMock;

    @Mock
    BirthdayMessageService birthdayEmailMessageServiceImplMock;

    @Mock
    BirthdayMessageService birthdayPhoneMessageServiceImplMock;

    BirthdayScheduler birthdayScheduler;

    @BeforeEach
    void setUp() {
        birthdayScheduler = new BirthdayScheduler(fileReaderServiceMock, userServiceMock,
                birthdayEmailMessageServiceImplMock, birthdayPhoneMessageServiceImplMock);
    }

    @Test
    void shouldSendMessages() {
        List<User> users = getUsers();

        when(fileReaderServiceMock.getBirthdayUsersFromFile("birthdays.txt")).thenReturn(users);
        when(userServiceMock.findAllUsers()).thenReturn(users);
        lenient().when(birthdayPhoneMessageServiceImplMock.sendBirthDayMessage(anyString(), anyString())).thenReturn("OK");
        lenient().when(birthdayEmailMessageServiceImplMock.sendBirthDayMessage(anyString(), anyString())).thenReturn("OK");

        birthdayScheduler.sendMessages();

        verify(birthdayEmailMessageServiceImplMock, times(users.size())).sendBirthDayMessage(anyString(), anyString());

        // TODO - Check why this fails
        //verify(birthdayPhoneMessageServiceImplMock, times(users.size())).sendBirthDayMessage(anyString(), anyString());
    }

    private List<User> getUsers() {
        User userBirthday = User.builder()
                .birthDay(LocalDate.now())
                .firstName("John")
                .lastName("Doe")
                .email("john.doe@hotmail.com")
                .build();
        User userOneNoBirthday = User.builder()
                .firstName("David")
                .lastName("Hernandez")
                .email("david.hernandez@outlook.com")
                .birthDay(LocalDate.now().plusDays(2))
                .build();
        User userTwoNoBirthday = User.builder()
                .firstName("Rose")
                .lastName("Shepard")
                .email("rose.shepard@gmail.com")
                .birthDay(LocalDate.now().minusDays(2))
                .build();

        return Arrays.asList(userBirthday, userOneNoBirthday, userTwoNoBirthday);
    }

}