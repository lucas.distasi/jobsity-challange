package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.entity.UserEntity;
import com.distasilucas.jobsitychallange.mapper.EntityMapper;
import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.repository.UserRepository;
import com.distasilucas.jobsitychallange.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static com.distasilucas.jobsitychallange.constants.Constants.BIRTHDAY_DATE_FORMAT;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    UserRepository userRepositoryMock;

    @Mock
    EntityMapper<User, UserEntity> userUserEntityEntityMapperMock;

    UserService userService;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(userRepositoryMock, userUserEntityEntityMapperMock);
    }

    @Test
    void shouldRetrieveAllUsers() {
        List<UserEntity> userEntities = getUsers();
        User user = getUser();

        when(userRepositoryMock.findAll()).thenReturn(userEntities);
        when(userUserEntityEntityMapperMock.mapFrom(any(UserEntity.class))).thenReturn(user);

        List<User> users = userService.findAllUsers();

        assertAll(
                () -> assertFalse(CollectionUtils.isEmpty(users)),
                () -> assertEquals(2, users.size()),
                () -> assertEquals(user.getFirstName(), users.get(0).getFirstName())
        );
    }

    private List<UserEntity> getUsers() {
        UserEntity userBirthDay = getUserBirthDay();

        UserEntity userNotBirthDay = new UserEntity();
        userNotBirthDay.setBirthDay(LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern(BIRTHDAY_DATE_FORMAT)));
        userNotBirthDay.setFirstName("Elon");

        return Arrays.asList(userBirthDay, userNotBirthDay);
    }

    private UserEntity getUserBirthDay() {
        UserEntity userBirthDay = new UserEntity();
        userBirthDay.setBirthDay(LocalDate.now().format(DateTimeFormatter.ofPattern(BIRTHDAY_DATE_FORMAT)));
        userBirthDay.setFirstName("John");

        return userBirthDay;
    }

    private User getUser() {
        return User.builder()
                .firstName("John")
                .birthDay(LocalDate.now())
                .build();
    }

}