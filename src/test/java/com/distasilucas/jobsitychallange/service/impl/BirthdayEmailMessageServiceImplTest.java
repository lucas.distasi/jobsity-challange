package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.providers.email.GmailProviderService;
import com.distasilucas.jobsitychallange.service.providers.email.HotmailProviderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.distasilucas.jobsitychallange.constants.Constants.EMAIL_BIRTHDAY_CONTENT;
import static com.distasilucas.jobsitychallange.constants.Constants.EMAIL_BIRTHDAY_SUBJECT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BirthdayEmailMessageServiceImplTest {

    @Mock
    GmailProviderService gmailProviderServiceMock;

    @Mock
    HotmailProviderService hotmailProviderServiceMock;

    BirthdayMessageService birthdayMessageService;

    @BeforeEach
    void setUp() {
        birthdayMessageService = new BirthdayEmailMessageServiceImpl(gmailProviderServiceMock);
    }

    @Test
    void shouldSendGmailMessage() {
        String destination = "john.doe@hotmail.com";
        String emailContent = String.format(EMAIL_BIRTHDAY_CONTENT, destination);

        when(gmailProviderServiceMock.sendEmail(destination, EMAIL_BIRTHDAY_SUBJECT, emailContent)).thenReturn("OK-Gmail");

        String response = birthdayMessageService.sendBirthDayMessage(destination, emailContent);

        assertEquals("OK-Gmail", response);
    }

    @Test
    void shouldSendHotmailMessage() {
        String destination = "john.doe@hotmail.com";
        String emailContent = String.format(EMAIL_BIRTHDAY_CONTENT, destination);
        birthdayMessageService = new BirthdayEmailMessageServiceImpl(hotmailProviderServiceMock);

        when(hotmailProviderServiceMock.sendEmail(destination, EMAIL_BIRTHDAY_SUBJECT, emailContent)).thenReturn("OK-Hotmail");

        String response = birthdayMessageService.sendBirthDayMessage(destination, emailContent);

        assertEquals("OK-Hotmail", response);
    }
}
