package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.model.User;
import com.distasilucas.jobsitychallange.service.FileReaderService;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileReaderServiceImplTest {

    FileReaderService fileReaderService = new FileReaderServiceImpl();

    @Test
    void shouldReturnAllUsers() {
        List<User> users = fileReaderService.getBirthdayUsersFromFile("birthdays.txt");

        assertAll(
                () -> assertTrue(users.size() > 0),
                () -> assertEquals(59, users.size())
        );
    }

    @Test
    void shouldReturnEmptyListIfFileIsNotFound() {
        List<User> users = fileReaderService.getBirthdayUsersFromFile("users.txt");

        assertTrue(CollectionUtils.isEmpty(users));
    }

}