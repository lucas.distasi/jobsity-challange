package com.distasilucas.jobsitychallange.service.impl;

import com.distasilucas.jobsitychallange.service.BirthdayMessageService;
import com.distasilucas.jobsitychallange.service.providers.phone.TelesignProviderService;
import com.distasilucas.jobsitychallange.service.providers.phone.TwilioProviderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BirthdayPhoneMessageServiceImplTest {

    @Mock
    TwilioProviderService twilioProviderServiceMock;

    @Mock
    TelesignProviderService telesignProviderServiceMock;

    BirthdayMessageService birthdayMessageService;

    @BeforeEach
    void setUp() {
        birthdayMessageService = new BirthdayPhoneMessageServiceImpl(twilioProviderServiceMock);
    }

    @Test
    void shouldSendMessageThroughTwilio() {
        when(twilioProviderServiceMock.sendMessage("destination", "content")).thenReturn("OK-Twilio");

        String message = birthdayMessageService.sendBirthDayMessage("destination", "content");

        assertEquals("OK-Twilio", message);
    }

    @Test
    void shouldSendMessageThroughTelesign() {
        birthdayMessageService = new BirthdayPhoneMessageServiceImpl(telesignProviderServiceMock);

        when(telesignProviderServiceMock.sendMessage("destination", "content")).thenReturn("OK-Telesign");

        String message = birthdayMessageService.sendBirthDayMessage("destination", "content");

        assertEquals("OK-Telesign", message);
    }

}
