package com.distasilucas.jobsitychallange.service.providers.email;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HotmailProviderServiceTest {

    HotmailProviderService hotmailProviderService = new HotmailProviderService();

    @Test
    void shouldSendHotmailEmail() {
        String hotmailResponse = hotmailProviderService.sendEmail("destination", "subject", "content");

        assertEquals("HotmailProviderService", hotmailResponse);
    }
}
