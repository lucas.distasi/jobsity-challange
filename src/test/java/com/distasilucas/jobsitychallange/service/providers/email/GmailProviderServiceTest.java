package com.distasilucas.jobsitychallange.service.providers.email;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GmailProviderServiceTest {

    GmailProviderService gmailProviderService = new GmailProviderService();

    @Test
    void shouldSendGmailEmail() {
        String gmailServiceResponse = gmailProviderService.sendEmail("destination", "subject", "content");

        assertEquals("GmailProviderService", gmailServiceResponse);
    }
}
