package com.distasilucas.jobsitychallange.service.providers.phone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TelesignProviderServiceTest {

    TelesignProviderService telesignProviderService = new TelesignProviderService();

    @Test

    void shouldSendMessageThroughTelesign() {
        String message = telesignProviderService.sendMessage("destination", "content");

        assertEquals("TelesignProviderService", message);
    }
}
