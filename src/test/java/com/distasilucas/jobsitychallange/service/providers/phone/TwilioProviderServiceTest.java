package com.distasilucas.jobsitychallange.service.providers.phone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TwilioProviderServiceTest {

    TwilioProviderService twilioProviderService = new TwilioProviderService();

    @Test
    void shouldSendMessageThroughTwilio() {
        String message = twilioProviderService.sendMessage("destination", "content");

        assertEquals("TwilioProviderService", message);
    }
}
