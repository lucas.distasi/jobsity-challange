# Birthdays App

Birthdays App it's a Java-Spring application that reads user information from a text file
and a database.

Users from the file will receive an Email reminder or a birthday wish if it's him/her birthday.
Users from the database will receive an sms message to their phones.

Nothing is needed to send the messages.

A cron will run every day at noon and send the birthday messages and birthday messages reminder.

## How To

In order to run this you need to follow the below steps.

1. Unzip the zip file.
2. Navigate to the folder project (_jobsity-challange_).
3. Run `./mvnw clean install`. This should generate a _target_ folder.
4. Navigate to the generated _target_ folder.
5. Run `java -jar jobsity-challange-1.0.0-SNAPSHOT.jar`.